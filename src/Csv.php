<?php

namespace Sergterr\Csv;

/**
 * Csv Library
 *
 * Version 1.0.10
 */
class Csv
{

    public function __construct()
    {
        // Nothing
    }

    /**
     * Reads a CSV file and returns the data as an array.
     * Читает CSV файл и возвращает данные в виде массива.
     *
     * @param $file_path // string Путь до csv файла.
     * @param string[] $file_encodings
     * @param string $col_delimiter Разделитель колонки (по умолчанию автоопределение)
     * @param string $row_delimiter Разделитель строки (по умолчанию автоопределение)
     *
     * @return array
     *
     * ver 6
     */
    public static function parseCsvFile(
        $file_path,
        array $file_encodings = ['cp1251', 'UTF-8'],
        string $col_delimiter = '',
        string $row_delimiter = ""
    ): array
    {
        if (empty($file_path) or !file_exists($file_path)) {
            return [
                'type' => 'error',
                'data' => 'Could not find file path'
            ];
        }

        $cont = trim(file_get_contents($file_path));

        $encoded_cont = mb_convert_encoding($cont, 'UTF-8', mb_detect_encoding($cont, $file_encodings));

        unset($cont);

        // определим разделитель
        if (!$row_delimiter) {
            $row_delimiter = "\r\n";
            if (false === strpos($encoded_cont, "\r\n")) {
                $row_delimiter = "\n";
            }
        }

        $lines = explode($row_delimiter, trim($encoded_cont));
        $lines = array_filter($lines);
        $lines = array_map('trim', $lines);

        // Авто-определим разделитель из двух возможных: ';' или ','.
        // Для расчета берем не больше 30 строк
        if (!$col_delimiter) {
            $lines10 = array_slice($lines, 0, 30);

            // если в строке нет одного из разделителей, то значит другой точно он...
            foreach ($lines10 as $line) {
                if (!strpos($line, ',')) {
                    $col_delimiter = ';';
                }
                if (!strpos($line, ';')) {
                    $col_delimiter = ',';
                }

                if ($col_delimiter) {
                    break;
                }
            }

            // Если первый способ не дал результатов, то погружаемся в задачу и считаем кол разделителей в каждой строке.
            // Где больше одинаковых количеств найденного разделителя, тот и разделитель...
            if (!$col_delimiter) {
                $delim_counts = [';' => [], ',' => []];
                foreach ($lines10 as $line) {
                    $delim_counts[','][] = substr_count($line, ',');
                    $delim_counts[';'][] = substr_count($line, ';');
                }

                $delim_counts = array_map('array_filter', $delim_counts); // уберем нули

                // кол-во одинаковых значений массива - это потенциальный разделитель
                $delim_counts = array_map('array_count_values', $delim_counts);

                $delim_counts = array_map('max', $delim_counts); // Берем только макс. значения вхождений

                if ($delim_counts[';'] === $delim_counts[',']) {
                    return [
                        'type' => 'error',
                        'data' => 'Could not determine column separator.'
                    ];
                }

                $col_delimiter = array_search(max($delim_counts), $delim_counts);
            }

        }

        $data = [
            'type' => 'success',
        ];
        foreach ($lines as $key => $line) {
            $data['data'][] = str_getcsv($line, $col_delimiter);
            unset($lines[$key]);
        }

        return $data;
    }

    /**
     * With Title
     * С заголовком
     *
     * @param string $file_path
     *
     * @return array
     */
    public static function parseCsvFileWithTitle(string $file_path = ''): array
    {
        $parse_file = self::parseCsvFile($file_path);

        if (!isset($parse_file['type'])) {
            return [];
        }
        if ('error' == $parse_file['type']) {
            return $parse_file;
        }

        $output = [];

        if (is_array($parse_file['data']) && 2 < count($parse_file['data'])) {
            foreach ($parse_file['data'] as $key => $line) {
                if (1 < $key) {
                    foreach ($line as $field_key => $field_value) {
                        $output[$key - 2][] = [
                            'col_name' => $parse_file['data'][0][$field_key],
                            'col_slug' => $parse_file['data'][1][$field_key],
                            'col_val' => $field_value,
                        ];
                    }
                }
            }
        }

        return $output;
    }

    /**
     * Создает CSV файл из переданных в массиве данных.
     *
     * @param array $create_data Массив данных из которых нужно создать CSV файл.
     * @param string|null $file Путь до файла 'path/to/test.csv'. Если не указать, то просто вернет результат.
     * @param string $col_delimiter
     * @param string $row_delimiter
     *
     * @return false|string            CSV строку или false, если не удалось создать файл.
     *
     * ver 2.0
     */
    public static function createCsvFile(
        array  $create_data,
        string $file = null,
        string $col_delimiter = ';',
        string $row_delimiter = "\r\n"
    )
    {
        if ($file && !is_dir(dirname($file))) {
            return false;
        }

        // строка, которая будет записана в csv файл
        $CSV_str = '';

        // перебираем все данные
        foreach ($create_data as $row) {
            $cols = array();

            foreach ($row as $col_val) {
                // Строки должны быть в кавычках ""
                // Кавычки " внутри строк нужно предварить такой же кавычкой "
                if ($col_val && preg_match('/[",;\r\n]/', $col_val)) {
                    // поправим перенос строки
                    if ($row_delimiter === "\r\n") {
                        $col_val = str_replace("\r\n", '\n', $col_val);
                        $col_val = str_replace("\r", '', $col_val);
                    } elseif ($row_delimiter === "\n") {
                        $col_val = str_replace("\n", '\r', $col_val);
                        $col_val = str_replace("\r\r", '\r', $col_val);
                    }

                    $col_val = str_replace('"', '""', $col_val); // предваряем "
                    $col_val = '"' . $col_val . '"'; // обрамляем в "
                }

                $cols[] = $col_val; // добавляем колонку в данные
            }

            $CSV_str .= implode($col_delimiter, $cols) . $row_delimiter; // добавляем строку в данные
        }

        $CSV_str = rtrim($CSV_str, $row_delimiter);

        // задаем кодировку windows-1251 для строки
        if ($file) {
            $CSV_str = mb_convert_encoding($CSV_str, 'UTF-8', mb_detect_encoding($CSV_str, ['cp1251', 'UTF-8']));

            // создаем csv файл и записываем в него строку
            $done = file_put_contents($file, $CSV_str);

            return $done ? $CSV_str : false;
        }

        return $CSV_str;

    }

}
